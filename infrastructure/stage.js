const merge = require('lodash.merge');
const heroin = require('heroin-js');

const configurator = heroin(process.env.HEROKU_API_TOKEN);

configurator(merge({}, require('./base'), {
    name: 'bjarneo-books-stage',
    config_vars: {
        NODE_ENV: 'staging'
    },
    domains: ['bjarneo-books-stage.herokuapp.com']
}));