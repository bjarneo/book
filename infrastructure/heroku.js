const heroin = require('heroin-js');

const configurator = heroin(process.env.HEROKU_API_TOKEN);

// configurator.export('bjarneo-books').then(res => console.log(res));

configurator({
    name: 'bjarneo-books-stage',
    region: 'eu',
    maintenance: false,
    stack: 'cedar-14',
    config_vars: {
        // MONGOLAB_URI: 'mongodb://heroku_pzk7dc7f:75t935lpto4q0jem92tgvga2ag@ds047514.mlab.com:47514/heroku_pzk7dc7f',
        NODE_ENV: 'production'
    },
    addons: {
        mongolab: { plan: 'mongolab:sandbox' }
    },
    collaborators: [ 'bjarne.overli@vg.no' ],
    features: {
        'runtime-dyno-metadata': { enabled: false },
        'log-runtime-metrics': { enabled: false },
        'http-session-affinity': { enabled: false },
        preboot: { enabled: false },
        'http-shard-header': { enabled: false },
        'http-end-to-end-continue': { enabled: false }
    },
    formation: [ { process: 'web', quantity: 1, size: 'Free' } ],
    log_drains: [],
    domains: [ 'bjarneo-books-stage.herokuapp.com' ]
});