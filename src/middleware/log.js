const logMiddleware = (req, res, next) => {
    console.log('incoming req %s', new Date());

    next();
};

module.exports = logMiddleware;