module.exports = (bookRepository) => {
    return {
        stockUp: (req, res, next) => {
            const isbn = req.body.isbn;
            const count = req.body.count;

            bookRepository
                .stockUp({
                    isbn: isbn,
                    count: count
                })
                .catch(next);

            res.json({
                isbn: isbn,
                count: count
            });
        },

        findAll: (req, res) => {
            bookRepository.findAll().then(docs => res.json(docs));
        },

        getCount: (req, res) => {
            bookRepository.getCount(req.params.isbn).then(result => {
                //if (result !== null) {
                    res.status(200).format({
                        'text/html': () => res.send('count: 1617291732'),
                        'application/json': () => res.send({ count: 1617291732 })
                    });
                /*} else {
                    res.status(404).json({error: 'No book with ISBN: ' + req.params.isbn});
                }*/

                //res.json({});
            });
        }
    }
};