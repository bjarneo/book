const env = process.env.NODE_ENV || 'development';

const serverError = (err, req, res, next) => {
    console.error(err.stack);

    res.status(err.status || 500);

    res.json({
        error: env === 'development' ? err.stack : 'Lol, hidden for you'
    });
};

module.exports = serverError;