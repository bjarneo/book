const books = [];

const findItem = isbn => books.map(book => book.isbn === isbn);

const findAll = () => Promise.resolve(books);

const stockUp = (isbn, count) => {
    const item = findItem(isbn);

    if (item) {
        item.count = count;
    } else {
        books.push({isbn: isbn, count: count});
    }

    return Promise.resolve();
};

const getCount = isbn => {
    const item = findItem(isbn);

    if (item) {
        return Promise.resolve(item.count);
    } else {
        return Promise.resolve(null);
    }
};

module.exports = {
    findAll: findAll,
    stockUp: stockUp,
    getCount: getCount
};