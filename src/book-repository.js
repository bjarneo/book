const mongoClient = require('mongodb').MongoClient;
const mongoHost = process.env.MONGOLAB_URI || 'mongodb://localhost:27017/nodecourse';
const db = mongoClient
        .connect(mongoHost)
        .then(db => db.collection('books'));

function stockUp(item) {
    return db.then(collection => {
        collection.insertOne({
            isbn: item.isbn,
            count: item.count
        });
    });
}

function findAll() {
    return db.then(collection => {
        return collection
            .find({})
            .toArray();
    });
}

function getCount(id) {
    return db.then(collection => {
        return collection.find({
            "isbn": id
        }).limit(1).next();
    }).then(result => {
        if (result) {
            return result.count;
        }

        return null;
    });
}

module.exports = {
    stockUp: stockUp,
    findAll: findAll,
    getCount: getCount
};