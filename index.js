const bookRepository = require('./src/book-repository');
const app = require('./app')(bookRepository, true); // last is require login
var port = process.env.PORT || 3000;

app.listen(port, () => console.log('Example app listening on port: %s!', port));
