const request = require('supertest');
const express = require('express');
const assert = require('assert');
const inMemoryRepository = require('../src/in-memory-repository');
const app = require('../app.js')(inMemoryRepository, false); // last is require login

describe('GET /stock', () => {
    it('should respond with json', (done) => {
        request(app)
            .post('/stock')
            .set('Content-Type', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });

    it('should return same isbn and stock on post', (done) => {
        request(app)
            .post('/stock')
            .send({
                  isbn: 1617291732,
                  count: 129
            })
            .set('Content-Type', 'application/json')
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                assert.equal(res.body.isbn, 1617291732);

                assert.equal(res.body.count, 129);

                done();
            });
    });
});
