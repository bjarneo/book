const shisha = require('shisha');

const resources = {
    'https://user:pass@bjarneo-books-stage.herokuapp.com/': 200,
    'https://user:pass@bjarneo-books-stage.herokuapp.com/list': 200,
};

shisha.smoke(resources, (res) => {
    res.forEach((page) => {
        if (!page.result) {
            process.exit(1);
        }
    });
});