const express = require('express');
const bodyParser = require('body-parser');
const clientError = require('./src/error/client-error');
const serverError = require('./src/error/server-error');
const logMiddleware = require('./src/middleware/log');
const auth = require('./src/middleware/auth');

module.exports = (bookRepository, requireLogin) => {
    const routes = require('./src/routes')(bookRepository);
    const app = express();

    app.use(bodyParser.json());

    if (requireLogin) {
        app.use(auth('user', 'pass'));
    }

    app.get('/', logMiddleware, (req, res, next) => res.send('Books!'));
    app.post('/stock', routes.stockUp);
    app.get('/stock/:isbn', routes.getCount);
    app.get('/list', routes.findAll);

    app.use(clientError);
    app.use(serverError);

    return app;
};